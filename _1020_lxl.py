import logging

import cqhttp
import telegram
import random
import traceback

import global_vars
from bot_constant import *
from main.command import command_listener
from main.utils import send_both_side, get_plugin_priority, get_forward_index

logger = logging.getLogger("CTB." + __name__)
logger.debug(__name__ + " loading")

sayings = [
    '法法',
    'gay',
    '女装',
    '喵',
    'H',
    '毒瘤',
    'prpr lxl'
]
trig_keywords = [
    '法法',
    'gay',
    '女装',
    '喵',
    '妹妹',
    'lxl',
    '发情',
    'prpr',
    '可爱',
    '变态'
]

random_fun = random.SystemRandom()

@command_listener('lxl', 'lxl', description='毒瘤 lxl')
def random_things(forward_index: int,
                  tg_group_id: int=None,
                  tg_user: telegram.User=None,
                  tg_message_id: int=None,
                  tg_reply_to: telegram.Message=None,
                  qq_group_id: int=None,
                  qq_discuss_id: int=None,
                  qq_user: int=None):

    message = random_fun.choice(sayings)
    global_vars.tg_bot.sendMessage(FORWARD_LIST[forward_index]['TG'], message)
    global_vars.qq_bot.send_group_msg(group_id=qq_group_id, message=message)

    return ''

@global_vars.qq_bot.on_message('group', 'discuss', group=get_plugin_priority(__name__))
def reply_to_lxl(context):
    qq_group_id = context.get('group_id')
    qq_discuss_id = context.get('discuss_id')
    forward_index = get_forward_index(qq_group_id=qq_group_id,
                                      qq_discuss_id=qq_discuss_id)
    logger.debug(__name__ + " qqid: " + str(context['user_id']))
    if context['user_id'] == 1974015903:
        flag = False
        for kwd in trig_keywords:
            for msg in context.get('message', []):
                if msg.get('type') == 'text' and kwd in msg.get('data', {}).get('text', ''):
                    logger.debug(__name__ + " FOUND trigger keyword: " + kwd)
                    flag = True
                    break
        if flag:
            message = '[lxlBOT] ' + random_fun.choice(sayings)
            global_vars.tg_bot.sendMessage(FORWARD_LIST[forward_index]['TG'], message)
            global_vars.qq_bot.send_group_msg(group_id=qq_group_id, message=message)

    return {'pass': True}